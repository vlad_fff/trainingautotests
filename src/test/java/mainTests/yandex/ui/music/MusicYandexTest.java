package mainTests.yandex.ui.music;

import mainTests.yandex.ui.mail.MailYandexBase;
import org.testng.annotations.*;

import static mainTests.ConsoleLogger.logToConsole;
import static mainTests.yandex.ui.WaitersAndJSExecutors.waitPageTitle;

public class MusicYandexTest extends MusicYandexBase {

    private static final String BASE_URL = "https://yandex.by/";
    private static MailYandexBase mailYandexBase;


    @BeforeMethod
    public void beforeMethod() {
        mailYandexBase = new MailYandexBase();

        driver.get(BASE_URL);

        waitPageTitle(driver, "Яндекс");
        logToConsole("Перешел на: %s", BASE_URL);

        logToConsole("Pre-Conditions - Login:");
        mailYandexBase.goToMail(driver);
        mailYandexBase.enterLogin(driver, "AutotestUser");
        mailYandexBase.enterPassword(driver, "AutotestUser123");

        waitPageTitle(driver, "Почта");
        logToConsole("Перешел на страницу: %s", driver.getTitle());

        driver.navigate().to(BASE_URL);
        waitPageTitle(driver, "Яндекс");
        logToConsole("Перешел на: %s", BASE_URL);
    }

    @AfterMethod
    public void afterMethod() {
        driver.navigate().to(BASE_URL);
        waitPageTitle(driver, "Яндекс");
        logToConsole("Перешел на: %s", BASE_URL);

        logToConsole("Post-Conditions - Logout:");
        mailYandexBase.mailLogoutOnMainPage(driver);
    }


    @Test
    public void checkArtist() {
        goToMusic(driver);
        inputSubstringForSearch(driver, "Metal");
        chooseItemDropDownMenu(driver, "Исполнители", "Metallica");
        checkArtist(driver, "Metallica");
        checkArtistOfPopularAlbums(driver, "Metallica");
    }


    @Ignore
    @Test(enabled = false)
    public void checkPlayingMusic() {
        goToMusic(driver);
        inputSubstringForSearch(driver, "beyo");
        chooseItemDropDownMenu(driver, "Исполнители", "Beyoncé");

        // Вызываем два раза, так как первый раз запускаем трек, а затем проверяем играет ли,
        // а второй раз по нажатию на ту же кнопку ставим на паузу и проверям приостановлен ли
        playOrPauseMusicInPopular(driver);
        checkMusicPlayingOrPaused(driver);

        //TODO: Дописать проверку на воспроизведение/приостановку трека (есть звук(и наоборот) или изменяется ли иконка)

        playOrPauseMusicInPopular(driver);
        checkMusicPlayingOrPaused(driver);


    }

}