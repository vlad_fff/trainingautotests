package sources.api.pojo.response.yandex;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class OperationYandexDisk {

    @JsonProperty("status")
    private String status;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperationYandexDisk that = (OperationYandexDisk) o;
        return Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status);
    }

    @Override
    public String toString() {
        return "OperationStatusYandexDisk{" +
                "status='" + status + '\'' +
                '}';
    }
}
