package sources.api.pojo.response.yandex;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkYandexDisk {

    @JsonProperty("href")
    private String href;
    @JsonProperty("method")
    private String method;
    @JsonProperty("templated")
    private boolean templated;


    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public boolean isTemplated() {
        return templated;
    }

    public void setTemplated(boolean templated) {
        this.templated = templated;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkYandexDisk that = (LinkYandexDisk) o;
        return templated == that.templated &&
                Objects.equals(href, that.href) &&
                Objects.equals(method, that.method);
    }

    @Override
    public int hashCode() {
        return Objects.hash(href, method, templated);
    }

    @Override
    public String toString() {
        return "LinkCreatedFolderYandexDisk{" +
                "href='" + href + '\'' +
                ", method='" + method + '\'' +
                ", templated=" + templated +
                '}';
    }
}
