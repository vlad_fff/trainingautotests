package sources.api.pojo.response.sw;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class PlanetSW {

    @JsonProperty("name")
    private String name;
    @JsonProperty("diameter")
    private String diameter;
    @JsonProperty("rotation_period")
    private String rotationPeriod;
    @JsonProperty("orbital_period")
    private String orbitalPeriod;
    @JsonProperty("gravity")
    private String gravity;
    @JsonProperty("population")
    private String population;
    @JsonProperty("climate")
    private String climate;
    @JsonProperty("terrain")
    private String terrain;
    @JsonProperty("surface_water")
    private String surfaceWater;
    @JsonProperty("residents")
    private List<String> residents;
    @JsonProperty("films")
    private List<String> films;
    @JsonProperty("url")
    private String url;
    @JsonProperty("created")
    private String created;
    @JsonProperty("edited")
    private String edited;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiameter() {
        return diameter;
    }

    public void setDiameter(String diameter) {
        this.diameter = diameter;
    }

    public String getRotation_period() {
        return rotationPeriod;
    }

    public void setRotation_period(String rotationPeriod) {
        this.rotationPeriod = rotationPeriod;
    }

    public String getOrbital_period() {
        return orbitalPeriod;
    }

    public void setOrbital_period(String orbitalPeriod) {
        this.orbitalPeriod = orbitalPeriod;
    }

    public String getGravity() {
        return gravity;
    }

    public void setGravity(String gravity) {
        this.gravity = gravity;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getClimate() {
        return climate;
    }

    public void setClimate(String climate) {
        this.climate = climate;
    }

    public String getTerrain() {
        return terrain;
    }

    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }

    public String getSurface_water() {
        return surfaceWater;
    }

    public void setSurface_water(String surfaceWater) {
        this.surfaceWater = surfaceWater;
    }

    public List<String> getResidents() {
        return residents;
    }

    public void setResidents(List<String> residents) {
        this.residents = residents;
    }

    public List<String> getFilms() {
        return films;
    }

    public void setFilms(List<String> films) {
        this.films = films;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEdited() {
        return edited;
    }

    public void setEdited(String edited) {
        this.edited = edited;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanetSW planet = (PlanetSW) o;
        return Objects.equals(name, planet.name) &&
                Objects.equals(diameter, planet.diameter) &&
                Objects.equals(rotationPeriod, planet.rotationPeriod) &&
                Objects.equals(orbitalPeriod, planet.orbitalPeriod) &&
                Objects.equals(gravity, planet.gravity) &&
                Objects.equals(population, planet.population) &&
                Objects.equals(climate, planet.climate) &&
                Objects.equals(terrain, planet.terrain) &&
                Objects.equals(surfaceWater, planet.surfaceWater) &&
                Objects.equals(residents, planet.residents) &&
                Objects.equals(films, planet.films) &&
                Objects.equals(url, planet.url) &&
                Objects.equals(created, planet.created) &&
                Objects.equals(edited, planet.edited);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, diameter, rotationPeriod, orbitalPeriod, gravity, population, climate, terrain, surfaceWater, residents, films, url, created, edited);
    }


    @Override
    public String toString() {
        return "Planet{" +
                "name='" + name + '\'' +
                ", diameter='" + diameter + '\'' +
                ", rotationPeriod='" + rotationPeriod + '\'' +
                ", orbitalPeriod='" + orbitalPeriod + '\'' +
                ", gravity='" + gravity + '\'' +
                ", population='" + population + '\'' +
                ", climate='" + climate + '\'' +
                ", terrain='" + terrain + '\'' +
                ", surfaceWater='" + surfaceWater + '\'' +
                ", residents=" + residents +
                ", films=" + films +
                ", url='" + url + '\'' +
                ", created='" + created + '\'' +
                ", edited='" + edited + '\'' +
                '}';
    }
}
