package mainTests.sw.api;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static mainTests.ConsoleLogger.logToConsole;

public class SWApiTest extends SWApiBase {

    @BeforeClass
    public static void beforeClass() {
        logToConsole("------------- Начинаю API-тестирование SW -------------");
    }


    @Test
    public void checkPlanetsCount() {
        checkCountOfPlanets(61);
    }

    @Test
    public void checkStatusCode() {
        checkStatusCodeByPlanetNumber(1, 200);
    }

    @Test
    public void checkGettingFilmNames() {
        getFilmNames();
    }

}
