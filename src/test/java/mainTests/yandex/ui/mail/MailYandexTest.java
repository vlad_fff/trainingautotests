package mainTests.yandex.ui.mail;

import mainTests.yandex.ui.WaitersAndJSExecutors;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static mainTests.ConsoleLogger.logToConsole;

public class MailYandexTest extends MailYandexBase {

    private static final String BASE_URL = "https://yandex.by/";


    @BeforeMethod
    public void beforeMethod() {
        driver.get(BASE_URL);

        WaitersAndJSExecutors.waitPageTitle(driver, "Яндекс");
        logToConsole("Перешел на: %s", BASE_URL);
    }


    @Test
    public void logout() {
        goToMail(driver);
        enterLogin(driver, "AutotestUser");
        enterPassword(driver, "AutotestUser123");
        logoutFromServices(driver);
    }

    @Test
    public void invalidPassword() {
        goToMail(driver);
        enterLogin(driver, "AutotestUser");
        enterPassword(driver, "NoAutotestUser123");
        enterCaptchaIfNecessary(driver, "NoCaptchaAnswer");
        checkValidationFieldOfAuthPage(driver);
    }

    @Test
    public void invalidLogin() {
        goToMail(driver);
        enterLogin(driver, "NoAutotestUser");
        checkValidationFieldOfAuthPage(driver);
    }
}
