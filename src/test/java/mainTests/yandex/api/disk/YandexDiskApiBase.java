package mainTests.yandex.api.disk;

import org.apache.http.HttpStatus;
import org.junit.Assert;
import sources.api.pojo.response.yandex.*;

import static mainTests.ConsoleLogger.logToConsole;
import static org.codehaus.groovy.runtime.DefaultGroovyMethods.sum;
import static org.codehaus.groovy.runtime.DefaultGroovyMethods.tr;
import static sources.api.YandexDiskApi.*;

public class YandexDiskApiBase {

    protected static int stepNumber = 1;


    /**
     * Создаем папку на диске
     * @param path
     */
    public LinkYandexDisk createFolderOnDisk(String path) {
        logToConsole("%s. Создаю папку \"%s\" на диске", stepNumber++, path);
        return createFolder(path);
    }

    /**
     * Удаляем папку/файл (перемещаем в корзину)
     * @param path
     */
    public LinkYandexDisk deleteFileOrFolderFromDisk(String path, String fileNameInTrash) {
        logToConsole("%s. Удаляю папку/файл \"%s\" с диска", stepNumber++, path);
        deleteFileOrFolderToTrash(path);
        return deleteFileOrFolderFromTrash(fileNameInTrash);
    }

    /**
     * Перемещаю папку/файл в корзину
     * @param path
     */
    public LinkYandexDisk moveFileOrFolderToTrash(String path) {
        logToConsole("%s. Перемещаю пустую папку/файл \"%s\" в корзину", stepNumber++, path);
        return deleteFileOrFolderToTrash(path);
    }

    /**
     * Попытка получить информацию о несуществующем файле/папке (генерация ошибки 404)
     * @param path
     */
    public void checkFileOrFolderIsNotExist(String path) {
        logToConsole("%s. Пытаюсь получить информацию о папке/файле \"%s\" на диске", stepNumber++, path);
        ErrorYandexDisk errorYandexDisk = getInfoNoExistFileOrFolder(path);
        logToConsole("Сообщение ответа: %s", errorYandexDisk.getMessage());
    }

    /**
     * Загружаем файл на диск
     * @param path
     */
    public void uploadFileToDisk(String path) {
        logToConsole("%s. Делаю запрос URL на загрузку файла \"%s\"", stepNumber++, path);
        LinkYandexDisk link = getUploadURL(path);

        logToConsole("%s. Получаю URL на загрузку файла", stepNumber++);
        String uploadURL = link.getHref();

        logToConsole("%s. Загружаю файл по полученному URL \"%s\"", stepNumber++, uploadURL);
        uploadFile(uploadURL);
    }

    /**
     * Восстанавливаем папку/файл из корзины
     * @param name
     */
    public LinkYandexDisk restoreFileOrFolderFromTrash(String name) {
        logToConsole("%s. Восстанавливаю папку/файл \"%s\" из корзины", stepNumber++, name);
        return restoreFileOrFolder(name);
    }

    /**
     * Получение метаданных ресурса и сравнение что тип параметров соответствует ожидаемому (name/type/path)
     * @param pathToResource
     * @param expectedName
     * @param expectedType
     * @param expectedPath
     */
    public void checkingPropertyTypesByMetaData(String pathToResource,
                                                String expectedName, String expectedType, String expectedPath) {

        logToConsole("%s. Получаю метаданные файла/папки \"%s\"", stepNumber++, pathToResource);
        ResourceYandexDisk resource = getFileOrFolderMetadata(pathToResource);

        String expectedFullPath = "disk:/" + expectedPath;

        String actualName = resource.getName();
        String actualType = resource.getType();
        String actualPath = resource.getPath();

        boolean resultName = expectedName.equals(actualName);
        boolean resultType = expectedType.equals(actualType);
        boolean resultPath = expectedFullPath.equals(actualPath);

        logToConsole("Expected Name: %s, Actual: %s, Equels: %s", expectedName, actualName, resultName);
        logToConsole("Expected Type: %s, Actual: %s, Equels: %s", expectedType, actualType, resultType);
        logToConsole("Expected Path: %s, Actual: %s, Equels: %s", expectedFullPath, actualPath, resultPath);

        Assert.assertTrue(resultName);
        Assert.assertTrue(resultType);
        Assert.assertTrue(resultPath);
    }

    /**
     * Получение метаданных корзины и проверка что наши папки и файлы были удалены (т.е корзина пуста)
     * @param expectedTotal
     */
    public void checkTrashIsEmpty(long expectedTotal) {
        logToConsole("%s. Получаю метаданные корзины", stepNumber++);
        ResourceYandexDisk resource = getTrashMetadata();

        logToConsole("Получаю количество ресурсов в корзине и проверяю соответствие ожидаемому результату");
        long actualTotal = resource.getEmbedded().getTotal();
        boolean result = expectedTotal == actualTotal;

        logToConsole("Expected: %s, Actual: %s, Equels: %s", expectedTotal, actualTotal, result);
        Assert.assertTrue(result);
    }

    /**
     * Полная очистка корзины
     */
    public LinkYandexDisk fullClearTrash() {
        logToConsole("%s. Очищаю корзину", stepNumber++);
        return clearTrash();
    }

    /**
     * Проверка изменения размера корзины
     * @param startTrashSize
     * @param endTrashSize
     * @param deletedFileSize
     */
    public void checkTrashSizeChanging(long startTrashSize, long endTrashSize, long... deletedFileSize) {
        logToConsole("%s. Проверяю: конечный размер корзины = начальный размер + размер добавленных файлов", stepNumber++);

        long sizeDeletedFiles = sum(deletedFileSize);
        long sumStartSizeAndDeletedFiles = startTrashSize + sizeDeletedFiles;

        boolean result = endTrashSize == sumStartSizeAndDeletedFiles;

        logToConsole("Начальный размер корзины: %s", startTrashSize);
        logToConsole("Размер перемещенных в корзину файлов: %s", sizeDeletedFiles);
        logToConsole("Конечный размер корзины: %s", endTrashSize);
        logToConsole("Сумма начального размера корзины и размера удаленных файлов: %s", sumStartSizeAndDeletedFiles);

        logToConsole("Expected: %s, Actual: %s, Equels: %s", endTrashSize, sumStartSizeAndDeletedFiles, result);
        Assert.assertTrue(result);
    }

    /**
     * Получаем инфо о диске пользователя
     * @return
     */
    public DiskInfoYandexDisk getDiskInformation() {
        logToConsole("%s. Получаю информацию о диске пользователя", stepNumber++);
        return getDiskInfo();
    }

    /**
     * Получаем инфо о диске пользователя
     * @param path
     * @return
     */
    public ResourceYandexDisk getFileOrFolderMetadataInfo(String path) {
        logToConsole("%s. Получаю метаданные папки/файла", stepNumber++);
        return getFileOrFolderMetadata(path);
    }

    /**
     * Получаем метаданные корзины
     * @return
     */
    public ResourceYandexDisk getTrashMetadataInfo() {
        logToConsole("%s. Получаю метаданные корзины", stepNumber++);
        return getTrashMetadata();
    }


    public void checkSuccessStatus(LinkYandexDisk link, String expectedStatus) {

        if (link.getHref() == null) {
            logToConsole("Объект Link = null");
            return;
        }

        boolean result;
        OperationYandexDisk operation = checkOperationStatus(link.getHref());

        logToConsole("Начинаю проверку статуса операции");

        String actualStatus = operation.getStatus();
        result = expectedStatus.equals(actualStatus);

        logToConsole("Expected: %s, Actual: %s, Equels: %s", expectedStatus, actualStatus, result);

        if (result) {
            logToConsole("Статус операции соответствует ожидаемому");
        }
        else if (actualStatus.equals("in-progress")) {
            logToConsole("Ожидаю статус SUCCESS");
            actualStatus = waitStatus(expectedStatus, link.getHref());
            result = expectedStatus.equals(actualStatus);
            logToConsole("Expected: %s, Actual: %s, Equels: %s", expectedStatus, actualStatus, result);
        }
        else {
            logToConsole("Статус операции не соответствует ожидаемому");
        }

        Assert.assertTrue(result);
    }

    private static String waitStatus(String expectedStatus, String operationHref) {
        int repair = 20;
        int timeout = 500;

        OperationYandexDisk operation;

        try {
            do {
                operation = checkOperationStatus(operationHref);

                if (operation.getStatus().equals(expectedStatus))
                    return operation.getStatus();

                repair--;
                Thread.sleep(timeout);
            } while (repair != 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new RuntimeException("Статус операции не соответствует ожидаемому");
    }

}
