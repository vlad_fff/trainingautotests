package sources.api.pojo.response.yandex;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceListYandexDisk {

    @JsonProperty("sort")
    private String sort;
    @JsonProperty("public_key")
    private String publicKey;
    @JsonProperty("items")
    private List<ResourceYandexDisk> items;
    @JsonProperty("limit")
    private int limit;
    @JsonProperty("offset")
    private int offset;
    @JsonProperty("path")
    private String path;
    @JsonProperty("total")
    private int total;


    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public List<ResourceYandexDisk> getItems() {
        return items;
    }

    public void setItems(List<ResourceYandexDisk> items) {
        this.items = items;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResourceListYandexDisk that = (ResourceListYandexDisk) o;
        return limit == that.limit &&
                offset == that.offset &&
                total == that.total &&
                Objects.equals(sort, that.sort) &&
                Objects.equals(publicKey, that.publicKey) &&
                Objects.equals(items, that.items) &&
                Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sort, publicKey, items, limit, offset, path, total);
    }

    @Override
    public String toString() {
        return "ResourceListYandexDisk{" +
                "sort='" + sort + '\'' +
                ", publicKey='" + publicKey + '\'' +
                ", items=" + items +
                ", limit=" + limit +
                ", offset=" + offset +
                ", path='" + path + '\'' +
                ", total=" + total +
                '}';
    }
}
