package mainTests.yandex.ui.market;

import mainTests.yandex.ui.DriverConfigurationBase;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static mainTests.ConsoleLogger.logToConsole;
import static mainTests.yandex.ui.WaitersAndJSExecutors.*;

public class MarketYandexBase extends DriverConfigurationBase {

    private List<String> titlesOfElements = new ArrayList<>();
    private static int countElements;

    /**
     * Переходим в маркет
     * @param driver
     */
    public void goToMarket(WebDriver driver) {
        WebElement marketBtn = waitPresenceElement(driver, By.cssSelector("a[data-id='market']"));
        logToConsole("Нажимаю: %s", marketBtn.getText());
        marketBtn.click();

        driver.close();
        Set<String> newTab = driver.getWindowHandles();
        driver.switchTo().window(newTab.iterator().next());

        waitPageTitle(driver, "Маркет");
        logToConsole("Перешел на страницу: %s", driver.getTitle());
    }

    /**
     * Ищем товар через строку поиска
     * (вводим название и кликаем на кнопку "найти")
     * @param driver
     * @param searchStr
     */
    public void searchItemsInMarket(WebDriver driver, String searchStr) {
        waitPresenceElement(driver, By.xpath("//input[@id='header-search']")).sendKeys(searchStr);
        logToConsole("Ввожу в строку поиска: %s", searchStr);

        WebElement searchBtn = waitPresenceElement(driver, By.xpath("//button[@type='submit']"));
        logToConsole("Нажимаю: %s", searchBtn.getText());
        searchBtn.click();
    }

    /**
     * Добавляем несколько первых элементов в список сравнения
     * (кол-во передаем в параметрах)
     * @param driver
     * @param howMuch
     */
    public void addSomeFirstItemsToCompare(WebDriver driver, int howMuch) {
        List<WebElement> marketItems = waitPresenceElements(driver, By.xpath("//div[@class='n-snippet-cell2__hover']/div/div/div"));
        List<WebElement> titleElements = waitPresenceElements(driver, By.xpath("//h3/a[@title]"));

        for (int i = 0; i < howMuch; i++) {
            marketItems.get(i).click();
            titlesOfElements.add(titleElements.get(i).getAttribute("title"));
            logToConsole("Добавил элемент к сравнению - %s", titleElements.get(i).getAttribute("title"));
        }
        logToConsole("Выбрано элементов для сравнения: %s", titlesOfElements.size());
    }

    /**
     * Переходим к списку сравнения
     * @param driver
     */
    public void goToComparePage(WebDriver driver) {
        waitClickableElement(driver, By.xpath("//a[@href='/compare?track=head']")).click();
        waitPageTitle(driver, "Сравнение");
        logToConsole("Перешел на страницу: %s", driver.getTitle());
    }

    /**
     * Проверяем правильное ли кол-во элементов
     * И наши ли элементы добавлены в список сравнения
     * @param driver
     */
    public void checkItemsToCompare(WebDriver driver) {
        List<WebElement> compareElements = waitPresenceElements(driver, By.xpath("//div[@class='n-compare-content__line i-bem n-compare-content__line_js_inited']/*"));
        WebElement countOfCompareElements = waitPresenceElement(driver, By.xpath("//span[@class='n-compare-categories__number']"));

        boolean result = true;

        if (waitAttributeToBe(driver, countOfCompareElements, "innerHTML", String.valueOf(titlesOfElements.size())))
            countElements = Integer.parseInt(countOfCompareElements.getAttribute("innerHTML"));

        if (countElements > 0) {
            logToConsole("Элементов в списке сравнений: %s", countElements);

            for (int i = 0; i < titlesOfElements.size(); i++) {

                result &= titlesOfElements.get(i).equals(compareElements.get(compareElements.size() - i - 1).getText());

                logToConsole("Проверяю список товаров выбранных для сравнения: ");
                logToConsole("Expected : %s", titlesOfElements.get(i));
                logToConsole("Actual   : %s", compareElements.get(titlesOfElements.size() - i - 1).getText());
                logToConsole("Equals   : %s", result);
            }
        } else {
            WebElement noProductsMessage = waitPresenceElement(driver, By.xpath("//div[@class='title title_size_18']"));

            result &= titlesOfElements.size() == countElements;

            logToConsole("Проверяю количество элементов в списке сравнения: ");
            logToConsole("Expected: %s, Actual: %s, Equals: %s", titlesOfElements.size(), countElements, result);

            if (result)
                logToConsole("Сообщение на странице - %s", noProductsMessage.getAttribute("innerHTML"));
        }
        Assert.assertTrue(result);
    }

    /**
     * Удаляем (поочередно) элементы из списка сравнения
     * (+ удаляем из списка, в который вносили название товара для проверки)
     * @param driver
     */
    public void deleteAllItemsFromCompare(WebDriver driver) {
        //List<WebElement> titleElements = waitPresenceElements(driver, By.xpath("//a[@class='n-compare-head__name link']"));
        List<WebElement> deleteElementsBtn = waitPresenceElements(driver, By.xpath("//span[@class='n-compare-head__close n-hint i-bem n-hint_js_inited']"));

        for (int i = 0; i < deleteElementsBtn.size(); i++) {
            logToConsole("Удаляю %s-й элемент из сравнения", i + 1);
            //titlesOfElements.remove(titleElements.get(i).getText());
            new Actions(driver).moveToElement(deleteElementsBtn.get(i)).perform();
            deleteElementsBtn.get(i).click();
        }
        titlesOfElements.clear();
    }

    /**
     * Открываю меню со всеми категориями Маркета
     * @param driver
     */
    public void goToAllCategories(WebDriver driver) {
        WebElement allCategoriesBtn = waitPresenceElement(driver, By.xpath("//div[@data-zone-name='all-categories']/button[@role='tab']"));
        logToConsole("Нажимаю: %s", allCategoriesBtn.findElement(By.xpath("./div/span")).getText());
        allCategoriesBtn.click();
    }

    /**
     * Навожу мышь на пункт меню
     * @param driver
     * @param btnText
     */
    public void moveToMenuItem(WebDriver driver, String btnText) {
        List<WebElement> menuAllCategories = waitVisibilityElements(driver, By.xpath("//div[@role='tablist' and @aria-orientation='vertical']/div/button/a/span"));
        WebElement menuItem = menuAllCategories.stream().filter(t -> t.getText().equals(btnText)).findFirst().get();

        logToConsole("Навожу на: %s", menuItem.getText());
        new Actions(driver).moveToElement(menuItem).perform();
    }

    /**
     * Переход по элементу списка
     * @param driver
     * @param btnText
     */
    public void goToElementOfMenuItem(WebDriver driver, String btnText) {
        List<WebElement> subMenuOfMenuItems = waitPresenceElements(driver, By.xpath("//div/ul/li/div/a"));
        WebElement subMenuItem = subMenuOfMenuItems.stream().filter(t -> t.getText().equals(btnText)).findFirst().get();

        logToConsole("Нажимаю: %s", subMenuItem.getText());
        subMenuItem.click();
    }

    /**
     * Нажал отсортировать по цене
     * @param driver
     */
    public void sortByPrice(WebDriver driver) {
        WebElement byPriceBtn = waitVisibilityElement(driver, By.xpath("//div[@class='n-filter-block_pos_left i-bem']/div[3]/a"));
        logToConsole("Нажимаю: Сортировать %s", byPriceBtn.getText());
        byPriceBtn.click();
    }

    /**
     * Проверяем правильность сортировки по цене и по убыванию
     * @param driver
     */
    public void checkReverseOrder(WebDriver driver) {
        waitPageRefreshedAndVisibilityElements(driver, By.xpath("//div[@class='n-snippet-card2__part n-snippet-card2__part_type_right']"));
        waitPageRefreshedAndVisibilityElements(driver, By.xpath("//div[@class='n-snippet-card2__top']"));
        waitPageRefreshedAndVisibilityElements(driver, By.xpath("//div[@class='n-snippet-card2__price']"));
        waitPageRefreshedAndVisibilityElements(driver, By.xpath("//div[@class='n-snippet-card2__main-price']"));
        waitPageRefreshedAndVisibilityElements(driver, By.xpath("//div[@class='n-snippet-card2__main-price-wrapper']"));

        List<WebElement> pricesWebElements = waitPageRefreshedAndVisibilityElements(driver, By.xpath("//div[@class='price']"));
        List<Double> pagePriceSorted = new ArrayList<>();
        List<Double> ourPriceSorted = new ArrayList<>();

        for (WebElement webElement : pricesWebElements) {
            String textOfElement = webElement.getAttribute("innerHTML");

            pagePriceSorted.add(Double.parseDouble(textOfElement
                    .replace(textOfElement.substring(textOfElement.indexOf("&nbsp;")), "")
                    .replace(" ", "")
                    .replace(",", ".")
                    .replace("от", "")));
        }

        ourPriceSorted.addAll(pagePriceSorted);
        ourPriceSorted.sort(Comparator.reverseOrder());

        logToConsole("Сверяю два списка цен отсортированных по убыванию (программно и на странице): ");
        boolean result = true;

        for (int i = 0; i < ourPriceSorted.size(); i++) {
            result &= ourPriceSorted.equals(pagePriceSorted);
            logToConsole("Expected: %.2f, Actual: %.2f, Equels: %s", ourPriceSorted.get(i), pagePriceSorted.get(i), result);
        }
        Assert.assertTrue(result);
        logToConsole("Сортировка элементов по цене (по убыванию) выполнена правильно");
    }

    /**
     * Ввод параметра фильтра товаров "По ширине"
     * @param driver
     * @param attribute
     * @param attributeValue
     * @param filterParameter
     */
    public void inputFilterParameter(WebDriver driver, String attribute, String attributeValue, String filterParameter) {
        List<WebElement> inputFilterElements = waitPresenceElements(driver, By.xpath("//div[@class='search-layout']//div/fieldset/div/ul/li/p/input"));
        WebElement inputFilterElement = inputFilterElements.stream().filter(t -> t.getAttribute(attribute).equals(attributeValue)).findFirst().get();

        scrollToWebElement(driver, inputFilterElement);
        inputFilterElement.sendKeys(filterParameter);
        logToConsole("Ввожу фильтр \"%s\": %s", attributeValue, filterParameter);
    }

    /**
     * Проверяю правильность фильтрации по ширине товара
     * @param driver
     * @param miniDescription
     * @param width
     */
    public void checkFilterByProductsWidth(WebDriver driver, String miniDescription, String width) {

        boolean result = true;

        List<WebElement> descriptionElements = waitPageRefreshedAndVisibilityElements(driver, By.xpath("//div[@class='n-snippet-card2__content']/ul/li"));

        List<String> widthAndHeightProducts = descriptionElements.stream()
                .filter(t -> t.getAttribute("innerHTML").contains(miniDescription))
                .map(t -> t.getAttribute("innerHTML").replace(" ", "").replace(miniDescription, ""))
                .collect(Collectors.toList());

        List<String> widthOfProducts = widthAndHeightProducts.stream()
                .map(t -> t.substring(t.indexOf(":") + 1, t.indexOf("х"))).collect(Collectors.toList());

        logToConsole("Проверяю что фильтр по ширине отсортировал правильно: ");

        for (String widthOfElem : widthOfProducts) {
            result &= Double.parseDouble(widthOfElem) <= Double.parseDouble(width);
            logToConsole("Expected: <= %.2f, Actual: %.2f, Equels: %s", Double.parseDouble(width), Double.parseDouble(widthOfElem), result);
        }
        logToConsole("Фильтрация по ширине товара выполнена правильно");
    }

}
