package mainTests.yandex.ui.music;

import mainTests.yandex.ui.DriverConfigurationBase;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Set;

import static mainTests.ConsoleLogger.logToConsole;
import static mainTests.yandex.ui.WaitersAndJSExecutors.*;

public class MusicYandexBase extends DriverConfigurationBase {

    //private static final String YANDEX_MUSIC_LINK = "https://music.yandex.by/";
    //private static final String PLAY_ICON_LINK = "i/TyMldxG7JTqTkaqgZVR6k9vV-D4.svg";
    //private static final String PAUSE_ICON_LINK = "i/ZeAW1VytSr4AVRiKJAGhtOIRSno.svg";


    /**
     * Переходим по ссылке "Музыка"
     * @param driver
     */
    public void goToMusic(WebDriver driver) {
        WebElement musicLink = waitPresenceElement(driver, By.cssSelector("a[data-id='music']"));
        logToConsole("Нажимаю: %s", musicLink.getText());
        musicLink.click();

        driver.close();
        Set<String> newTab = driver.getWindowHandles();
        driver.switchTo().window(newTab.iterator().next());

        waitPageTitle(driver, "Музыка");
        logToConsole("Перешел на страницу: %s", driver.getTitle());
    }

    /**
     * Вводим подстроку для поиска трека/исполнителя/альбома и т.п.
     * @param driver
     * @param searchSubstring
     */
    public void inputSubstringForSearch(WebDriver driver, String searchSubstring) {
        waitVisibilityElement(driver, By.xpath("//div[@class='head__search']/div/div/input")).sendKeys(searchSubstring);
        logToConsole("Ввожу в строку поиска: %s", searchSubstring);
    }

    /**
     * Выбираем из выпадающего списка ожидаемый результат
     * @param driver
     * @param partMenuFilter
     * @param expectedItem
     */
    public void chooseItemDropDownMenu(WebDriver driver, String partMenuFilter, String expectedItem) {

        int indexOfMenuPart;

        switch (partMenuFilter) {
            case "Исполнители":
                indexOfMenuPart = 1;
                break;
            case "Треки":
                indexOfMenuPart = 2;
                break;
            case "Альбомы":
                indexOfMenuPart = 3;
                break;
            case "Плейлисты":
                indexOfMenuPart = 4;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + partMenuFilter);
        }

        waitPageRefreshedAndVisibilityElement(driver, By.cssSelector(".d-suggest__popup-content.deco-popup-suggest-menu"));
        waitPageRefreshedAndVisibilityElement(driver, By.cssSelector(".d-suggest__entities"));
        waitPageRefreshedAndVisibilityElement(driver, By.cssSelector(".d-suggest__items.d-suggest__items_type_artist"));
        waitPageRefreshedAndVisibilityElements(driver, By.cssSelector(".d-suggest-item__wrapper-link"));
        waitPageRefreshedAndVisibilityElements(driver, By.xpath("//a[@class='d-suggest-item__wrapper-link']/.."));

        List<WebElement> menuParts
                = waitPageRefreshedAndVisibilityElements(driver,
                By.xpath("//div[" + indexOfMenuPart + "]/*/div[@class='d-suggest-item__title typo typo-suggest-item']/div"));
        WebElement itemOfMenuParts
                = waitPageRefreshedAndClickableElement(driver,
                menuParts.stream().filter(t -> t.getAttribute("innerText").equals(expectedItem)).findFirst().get());

        Assert.assertEquals(itemOfMenuParts.getAttribute("innerText"), expectedItem);

        logToConsole("Выбираю: %s", itemOfMenuParts.getAttribute("innerText"));
        WebElement btnItemOfMenuParts = waitClickableElement(driver, itemOfMenuParts.findElement(By.xpath("./../../a[@class='d-suggest-item__wrapper-link']")));
        btnItemOfMenuParts.click();
    }

    /**
     * Проверяем что исполнитель соответствует ожидаемому результату
     * @param driver
     * @param expectedResult
     */
    public void checkArtist(WebDriver driver, String expectedResult) {

        WebElement artistTitle = waitPresenceElement(driver, By.cssSelector(".page-artist__title.typo-h1.typo-h1_big"));

        logToConsole("Проверяю что в качестве Артиста - %s: ", expectedResult);
        String actualResult = artistTitle.getAttribute("innerText");
        boolean result = actualResult.contains(expectedResult);

        logToConsole("Expected: %s, Actual: %s, Equels: %s", expectedResult, actualResult, result);
        Assert.assertTrue(result);
    }

    /**
     * Проверяем что исполнитель в разделе "Популярные альбомы" соответствует ожидаемому результату
     * @param driver
     * @param expectedResult
     */
    public void checkArtistOfPopularAlbums(WebDriver driver, String expectedResult) {

        boolean result = true;

        List<WebElement> artistTitles = waitPresenceElements(driver, By.cssSelector(".album__artist.deco-typo-secondary.typo-add"));
        logToConsole("Проверяю что в \"Популярные альбомы\" в качестве Исполнителя - %s: ", expectedResult);

        for (WebElement element : artistTitles) {

            String actualResult = element.getAttribute("title");
            result &= actualResult.contains(expectedResult);

            logToConsole("Expected: %s, Actual: %s, Equels: %s", expectedResult, actualResult, result);
        }

        Assert.assertTrue(result);
    }

    /**
     * Воспроизводим первый трек в разделе "Популярные треки"
     * либо ставим на паузу воспроизведенный трек
     * @param driver
     */
    public void playOrPauseMusicInPopular(WebDriver driver) {
        WebElement btnListenToMusic = waitClickableElement(driver, By.xpath("//div[@class='d-generic-page-head__main-actions']/button"));
        WebElement btnText = waitVisibilityElement(driver, btnListenToMusic.findElement(By.xpath("./span[@class='button2__label']")));
        logToConsole("Нажимаю: %s", btnText.getText());
        btnListenToMusic.click();
    }

    /**
     * Проверяем играет/на паузе трек
     * @param driver
     */
    public void checkMusicPlayingOrPaused(WebDriver driver) {

        //TODO: Дописать проверку на воспроизведение/приостановку трека (есть звук(и наоборот) или изменяется ли иконка)

    }

}
