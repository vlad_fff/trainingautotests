package sources.api.pojo.response.yandex;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceYandexDisk {

    @JsonProperty("public_key")
    private String publicKey;
    @JsonProperty("public_url")
    private String publicUrl;
    @JsonProperty("_embedded")
    private ResourceListYandexDisk embedded;
    @JsonProperty("preview")
    private String preview;
    @JsonProperty("name")
    private String name;
    @JsonProperty("custom_properties")
    private String customProperties;
    @JsonProperty("created")
    private String created;
    @JsonProperty("modified")
    private String modified;
    @JsonProperty("path")
    private String path;
    @JsonProperty("origin_path")
    private String originPath;
    @JsonProperty("md5")
    private String md5;
    @JsonProperty("type")
    private String type;
    @JsonProperty("mime_type")
    private String mimeType;
    @JsonProperty("size")
    private long size;


    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPublicUrl() {
        return publicUrl;
    }

    public void setPublicUrl(String publicUrl) {
        this.publicUrl = publicUrl;
    }

    public ResourceListYandexDisk getEmbedded() {
        return embedded;
    }

    public void setEmbedded(ResourceListYandexDisk embedded) {
        this.embedded = embedded;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(String customProperties) {
        this.customProperties = customProperties;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getOriginPath() {
        return originPath;
    }

    public void setOriginPath(String originPath) {
        this.originPath = originPath;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResourceYandexDisk that = (ResourceYandexDisk) o;
        return size == that.size &&
                Objects.equals(publicKey, that.publicKey) &&
                Objects.equals(publicUrl, that.publicUrl) &&
                Objects.equals(embedded, that.embedded) &&
                Objects.equals(preview, that.preview) &&
                Objects.equals(name, that.name) &&
                Objects.equals(customProperties, that.customProperties) &&
                Objects.equals(created, that.created) &&
                Objects.equals(modified, that.modified) &&
                Objects.equals(path, that.path) &&
                Objects.equals(originPath, that.originPath) &&
                Objects.equals(md5, that.md5) &&
                Objects.equals(type, that.type) &&
                Objects.equals(mimeType, that.mimeType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(publicKey, publicUrl, embedded, preview, name, customProperties, created, modified, path, originPath, md5, type, mimeType, size);
    }

    @Override
    public String toString() {
        return "ResourceYandexDisk{" +
                "publicKey='" + publicKey + '\'' +
                ", publicUrl='" + publicUrl + '\'' +
                ", embedded=" + embedded +
                ", preview='" + preview + '\'' +
                ", name='" + name + '\'' +
                ", customProperties='" + customProperties + '\'' +
                ", created='" + created + '\'' +
                ", modified='" + modified + '\'' +
                ", path='" + path + '\'' +
                ", originPath='" + originPath + '\'' +
                ", md5='" + md5 + '\'' +
                ", type='" + type + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", size=" + size +
                '}';
    }
}
