package mainTests.yandex.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

public abstract class DriverConfigurationBase {

    protected WebDriver driver;
    private ThreadLocal<WebDriver> driverThreadLocal = new ThreadLocal<>();


    @AfterClass
    public void afterClass() {
        driver.quit();
        driverThreadLocal.remove();
    }

    @Parameters("browser")
    @BeforeClass
    public WebDriver initDriver(String browser) {

        driver = driverThreadLocal.get();

        if (driver == null) {
            if (browser.equalsIgnoreCase("chrome")) {

                System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver_80.0.3987.106.exe");
                driver = new ChromeDriver();
                driverThreadLocal.set(driver);
            }
            else if (browser.equalsIgnoreCase("firefox")) {

                System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver_0.26.0.exe");
                driver = new FirefoxDriver();
                driverThreadLocal.set(driver);
            }

            driver.manage().window().maximize();
            driver.manage().timeouts()
                    .implicitlyWait(10, TimeUnit.SECONDS)
                    .pageLoadTimeout(10, TimeUnit.SECONDS)
                    .setScriptTimeout(10, TimeUnit.SECONDS);
        }

        return driver;
    }

}
