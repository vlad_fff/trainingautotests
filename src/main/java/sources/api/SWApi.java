package sources.api;

import org.apache.http.HttpStatus;
import sources.api.pojo.response.sw.FilmsSW;
import sources.api.pojo.response.sw.PlanetsSW;

import static io.restassured.RestAssured.given;

public class SWApi {

    private static final String API_BASE_URL = "https://swapi.co/api/";


    /**
     * Получаем инфу по планетам
     * @return
     */
    public static PlanetsSW getPlanetsInfo() {
        return given()
                .contentType("application/json")
                .baseUri(API_BASE_URL)
                .basePath("/planets")
               .when()
                .get()
               .then().statusCode(HttpStatus.SC_OK).extract().as(PlanetsSW.class);
    }

    /**
     * Получаем Status Code по URL
     * @param url
     * @return
     */
    public static int getStatusCodeByPlanetUrl(String url) {
        return given()
                .contentType("application/json")
                .baseUri(url)
               .when()
                .get().statusCode();
    }

    /**
     * Получаем инфу по фильмам
     * @return
     */
    public static FilmsSW getFilmsInfo() {
        return given()
                .contentType("application/json")
                .baseUri(API_BASE_URL)
                .basePath("/films")
               .when()
                .get()
               .then().statusCode(HttpStatus.SC_OK).extract().as(FilmsSW.class);
    }
}
