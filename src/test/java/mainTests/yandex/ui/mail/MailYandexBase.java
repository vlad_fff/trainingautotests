package mainTests.yandex.ui.mail;

import mainTests.yandex.ui.DriverConfigurationBase;
import mainTests.yandex.ui.WaitersAndJSExecutors;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Set;

import static mainTests.ConsoleLogger.logToConsole;

public class MailYandexBase extends DriverConfigurationBase {


    /**
     * Нажал на кнопку "Войти в почту"
     * Получил список элементов по xpath //a[@data-statlog='notifications.mail.logout.domik.login.big'],
     * а после проверки элемента на текст "Другой аккаунт" при наличии нажал на него
     * @param driver
     */
    public void goToMail(WebDriver driver) {
        WebElement enterToMail = WaitersAndJSExecutors.waitClickableElement(driver, By.xpath("//a[@data-statlog='notifications.mail.logout.domik.login.big']"));
        logToConsole("Нажимаю: %s", enterToMail.getText());
        enterToMail.click();

        driver.close();
        Set<String> newTab = driver.getWindowHandles();
        driver.switchTo().window(newTab.iterator().next());

        WaitersAndJSExecutors.waitPageTitle(driver, "Авторизация");
        logToConsole("Перешел на страницу: %s", driver.getTitle());

        List<WebElement> authHeaderElements = WaitersAndJSExecutors.waitPresenceElements(driver, By.xpath("//div[@class='passp-auth-header']/a[@tabindex='0']"));

        for (WebElement webElement : authHeaderElements) {
            if (webElement.isDisplayed() && webElement.getText().equals("Другой аккаунт")) {
                logToConsole("Нажимаю: %s", webElement.getText());
                webElement.click();
            }
        }
    }

    /**
     * Ввел "Логин" и нажал "Войти"
     * @param driver
     * @param login
     */
    public void enterLogin(WebDriver driver, String login) {
        WebElement loginField = WaitersAndJSExecutors.waitVisibilityElement(driver, By.xpath("//div/input[@id='passp-field-login']"));
        logToConsole("Ввожу логин: %s", login);
        loginField.sendKeys(login);

        WebElement enterBtn = WaitersAndJSExecutors.waitClickableElement(driver, By.xpath("//button[@type='submit']"));
        logToConsole("Нажимаю: %s", enterBtn.getText());
        enterBtn.click();
    }

    /**
     * Ввел "Пароль" и нажал "Войти"
     * @param driver
     * @param password
     */
    public void enterPassword(WebDriver driver, String password) {
        WebElement passwordField = WaitersAndJSExecutors.waitVisibilityElement(driver, By.xpath("//div/input[@id='passp-field-passwd']"));
        logToConsole("Ввожу пароль: %s", password);
        passwordField.sendKeys(password);

        WebElement enterBtn = WaitersAndJSExecutors.waitClickableElement(driver, By.xpath("//button[@type='submit']"));
        logToConsole("Нажимаю: %s", enterBtn.getText());
        enterBtn.click();
    }

    /**
     * Ввод captch'ы при необходимости
     * @param driver
     * @param captcha
     */
    public void enterCaptchaIfNecessary(WebDriver driver, String captcha) {
        WebElement passwordField = WaitersAndJSExecutors.waitPresenceElement(driver, By.cssSelector("input#passp-field-passwd"));

        if (waitFieldAttributeChangesToEmpty(passwordField, "value")) return;
        else if (WaitersAndJSExecutors.waitVisibilityElement(driver, By.cssSelector("img.captcha__image")).isDisplayed())
        {
            List<WebElement> authInputElements
                    = WaitersAndJSExecutors.waitPresenceElements(driver, By.cssSelector(".passp-form-field__input input"));

            WebElement captchaField = WaitersAndJSExecutors.waitVisibilityElement(driver,
                    authInputElements.stream().filter(t -> t.getAttribute("name").equals("captcha_answer")).findFirst().get());

            logToConsole("Ввожу: %s", captcha);
            captchaField.sendKeys(captcha);
        }

        WebElement enterBtn = WaitersAndJSExecutors.waitClickableElement(driver, By.xpath("//button[@type='submit']"));
        logToConsole("Нажимаю: %s", enterBtn.getText());
        enterBtn.click();
    }

    /**
     * Выход из сервисов Yandex'a
     * @param driver
     */
    public void logoutFromServices(WebDriver driver) {
        WaitersAndJSExecutors.waitPageTitle(driver, "Почта");
        logToConsole("Перешел на страницу: %s", driver.getTitle());

        WebElement headUserName = WaitersAndJSExecutors.waitClickableElement(driver, By.xpath("//div[@data-key='view=head-user']"));
        logToConsole("Нажимаю: %s", headUserName.getText().replace("\n", "_"));
        headUserName.click();

        WebElement signout = WaitersAndJSExecutors.waitClickableElement(driver, By.xpath("//div/a[@data-metric='Sign out of Yandex services']"));
        logToConsole("Нажимаю: %s", signout.getText());
        signout.click();

        WaitersAndJSExecutors.waitPageTitle(driver, "Яндекс");
        logToConsole("Logout выполнен");
    }

    /**
     * Выход из почты с главной страницы https://yandex.by
     * @param driver
     */
    public void mailLogoutOnMainPage(WebDriver driver) {
        WebElement homeLinkUserCard = WaitersAndJSExecutors.waitPresenceElement(driver, By.xpath("//a[@class='home-link usermenu-link__control home-link_black_yes']"));
        logToConsole("Нажимаю: %s", homeLinkUserCard.findElement(By.xpath("./span")).getText());
        homeLinkUserCard.click();

        WebElement btnLogout = WaitersAndJSExecutors.waitClickableElement(driver, By.xpath("//li/a[@data-statlog='mail.login.usermenu.exit']"));
        logToConsole("Нажимаю: %s", btnLogout.getAttribute("aria-label"));
        btnLogout.click();
    }

    /**
     * Проверка валидационных сообщений
     * @param driver
     */
    public void checkValidationFieldOfAuthPage(WebDriver driver) {
        WebElement errorField = WaitersAndJSExecutors.waitVisibilityElement(driver, By.xpath("//div[@class='passp-form-field__error']"));
        Assert.assertTrue(errorField.isDisplayed());
        logToConsole("Валидационное сообщение: %s\n", errorField.getText());
    }

    /**
     * Ожидание изменения атрибута элемента до пустого
     * @param element
     * @param attribute
     * @return
     */
    public static boolean waitFieldAttributeChangesToEmpty(WebElement element, String attribute) {
        int timeout = 100;
        int repeat = 5;
        try {
            do {
                if (element.getAttribute(attribute).isEmpty()) return true;
                repeat--;
                Thread.sleep(timeout);
            } while (repeat != 0);
        }
        catch (Exception e) {
            e.getStackTrace();
        }
        return false;
    }
}
