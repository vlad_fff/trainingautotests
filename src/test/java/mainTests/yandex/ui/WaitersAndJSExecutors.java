package mainTests.yandex.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class WaitersAndJSExecutors {

    /**
     * ----- Weiters -----
     */
    public static WebElement waitVisibilityElement(WebDriver driver, By by) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static WebElement waitVisibilityElement(WebDriver driver, WebElement webElement) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(webElement));
    }

    public static List<WebElement> waitVisibilityElements(WebDriver driver, By by) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }

    public static List<WebElement> waitVisibilityElements(WebDriver driver, List<WebElement> webElements) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfAllElements(webElements));
    }

    public static WebElement waitPresenceElement(WebDriver driver, By by) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public static List<WebElement> waitPresenceElements(WebDriver driver, By by) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    public static WebElement waitClickableElement(WebDriver driver, By by) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(by));
    }

    public static WebElement waitClickableElement(WebDriver driver, WebElement webElement) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static List<WebElement> waitPageRefreshedAndVisibilityElements(WebDriver driver, By by) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOfAllElementsLocatedBy(by)));
    }

    public static WebElement waitPageRefreshedAndVisibilityElement(WebDriver driver, By by) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOfElementLocated(by)));
    }

    public static WebElement waitPageRefreshedAndClickableElement(WebDriver driver, WebElement element) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));
    }

    public static WebElement waitPageRefreshedPresenceElement(WebDriver driver, By by) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.refreshed(ExpectedConditions.presenceOfElementLocated(by)));
    }

    public static List<WebElement> waitPageRefreshedPresenceElements(WebDriver driver, By by) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.refreshed(ExpectedConditions.presenceOfAllElementsLocatedBy(by)));
    }

    public static boolean waitPageRefreshedAttributeContains(WebDriver driver, WebElement webElement, String attributeName, String attributeValue) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.refreshed(ExpectedConditions.attributeContains(webElement, attributeName, attributeValue)));
    }

    public static void waitPageTitle(WebDriver driver, String title) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.titleContains(title));
    }

    public static boolean waitAttributeToBe(WebDriver driver, WebElement webElement, String attributeName, String attributeValue) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.attributeToBe(webElement, attributeName, attributeValue));
    }

    public static boolean waitAttributeContains(WebDriver driver, WebElement webElement, String attributeName, String attributeValue) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.attributeContains(webElement, attributeName, attributeValue));
    }

    public static boolean waitAttributeToBeNotEmpty(WebDriver driver, WebElement webElement, String attributeName) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.attributeToBeNotEmpty(webElement, attributeName));
    }

    public static boolean waitTextToBePresentInElement(WebDriver driver, WebElement webElement, String attributeValue) {
        return (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.textToBePresentInElement(webElement, attributeValue));
    }


    /**
     * ----- JS Executors -----
     */
    public static void scrollToWebElement(WebDriver driver, WebElement element) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", element);
    }


}
