package mainTests.yandex.ui.market;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static mainTests.ConsoleLogger.logToConsole;
import static mainTests.yandex.ui.WaitersAndJSExecutors.waitPageTitle;

public class MarketYandexTest extends MarketYandexBase {

    private static final String BASE_URL = "https://yandex.by/";


    @BeforeMethod
    public void beforeMethod() {
        driver.get(BASE_URL);

        waitPageTitle(driver, "Яндекс");
        logToConsole("Перешел на: %s", BASE_URL);
    }


    @Test
    public void deleteSelectedItems() {
        goToMarket(driver);
        searchItemsInMarket(driver, "Note 8");
        addSomeFirstItemsToCompare(driver, 2);
        goToComparePage(driver);
        checkItemsToCompare(driver);
        deleteAllItemsFromCompare(driver);
        checkItemsToCompare(driver);
    }

    @Test
    public void sortByPriceDesc() {
        goToMarket(driver);
        goToAllCategories(driver);
        moveToMenuItem(driver, "Электроника");
        goToElementOfMenuItem(driver, "Экшн-камеры");
        // Сортировку вызываем два раза, тк при 1ом нажатии сортируется "По возрастанию", а при 2ом нажатии меняется на "По убыванию"
        sortByPrice(driver);
        sortByPrice(driver);
        checkReverseOrder(driver);
    }

    @Test
    public void filterByWidth() {
        goToMarket(driver);
        goToAllCategories(driver);
        moveToMenuItem(driver, "Бытовая техника");
        goToElementOfMenuItem(driver, "Холодильники");
        inputFilterParameter(driver, "name", "Ширина до", "50");
        checkFilterByProductsWidth(driver, "ШхВхГ", "50");
    }
}
