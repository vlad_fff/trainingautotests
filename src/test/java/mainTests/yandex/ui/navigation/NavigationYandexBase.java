package mainTests.yandex.ui.navigation;

import mainTests.yandex.ui.DriverConfigurationBase;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static mainTests.ConsoleLogger.*;
import static mainTests.yandex.ui.WaitersAndJSExecutors.*;

public class NavigationYandexBase extends DriverConfigurationBase {


    /**
     * Нахожу элементы по xpath div class=home-arrow__tabs/div/*
     * Получаю список всех открытых вкладок
     * Поочередно перехожу в новую вкладку и проверяю соответствие каждой страницы
     *
     * @param driver
     */
    public void homeArrowTabsNavigation(WebDriver driver) {
        List<WebElement> homeTabsElements = waitPresenceElements(driver, By.xpath("//div[@role='navigation']/a"));
        List<String> textElements = homeTabsElements.stream().filter(t -> t.isDisplayed()).map(t -> t.getText()).collect(Collectors.toList());

        for (int i = 0; i < homeTabsElements.size() - 1; i++) {
            if (homeTabsElements.get(i).isDisplayed()) {
                homeTabsElements.get(i).sendKeys(Keys.chord(Keys.CONTROL, Keys.RETURN));
                logToConsole("Открываю в новой вкладке: %s", homeTabsElements.get(i).getText());
            }
        }


        ArrayList tabs = new ArrayList(driver.getWindowHandles());
        Set<String> newTab = driver.getWindowHandles();

        //for (int i = 1; i < newTab.size(); i++) {
        for (int i = 1; i < tabs.size(); i++) {
            driver.switchTo().window(tabs.get(tabs.size() - i).toString());
            //driver.switchTo().window(newTab.iterator().next());

            if (textElements.get(i - 1).contains("Видео"))
                textElements.set(i - 1, "Фильмы");

            boolean result = driver.getTitle().contains(textElements.get(i - 1));

            logToConsole("Перешел на: ");
            logToConsole("Expected : %s", textElements.get(i - 1));
            logToConsole("Actual   : %s", driver.getTitle());
            logToConsole("Equals   : %s", result);

            Assert.assertTrue(result);
        }
    }

}
