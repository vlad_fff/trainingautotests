package mainTests.sw.api;

import org.junit.Assert;
import sources.api.SWApi;
import sources.api.pojo.response.sw.*;

import java.util.ArrayList;
import java.util.List;

import static mainTests.ConsoleLogger.logToConsole;
import static sources.api.SWApi.*;

public class SWApiBase {


    /**
     * Проверяем соответствие количества планет
     * @param expectedCount
     */
    public void checkCountOfPlanets(int expectedCount) {
        logToConsole("Получаю информацию о планетах и их количество");
        PlanetsSW planets = getPlanetsInfo();
        int actualCount = planets.getCount();

        logToConsole("Проверяю соответствие количесва планет:");
        boolean result = expectedCount == actualCount;

        logToConsole("Expected: %s, Actual: %s, Equels: %s", expectedCount, actualCount, result);
        Assert.assertTrue(result);
    }

    /**
     * Проверяем соответствие Status Code'a
     * @param numberOfPlanet
     * @param expectedStatusCode
     */
    public void checkStatusCodeByPlanetNumber(int numberOfPlanet, int expectedStatusCode) {
        PlanetsSW planets = getPlanetsInfo();
        List<PlanetSW> listPlanets = planets.getResults();
        int actualCount = getStatusCodeByPlanetUrl(listPlanets.get(numberOfPlanet - 1).getUrl());

        logToConsole("Проверяю StatusCode по URL:");
        boolean result = expectedStatusCode == actualCount;

        logToConsole("Expected: %s, Actual: %s, Equels: %s", expectedStatusCode, actualCount, result);
        Assert.assertTrue(result);
    }

    /**
     * Получаем фильмы и ID эпизодов
     */
    public void getFilmNames() {
        FilmsSW films = getFilmsInfo();
        List<FilmSW> listFilms = new ArrayList<>(films.getResults());

        logToConsole("Получаю названия фильмов:");
        for (FilmSW film : listFilms) {
            logToConsole("Episode ID: %s, Film Name: %s", film.getEpisodeId(), film.getTitle());
        }
    }

}
