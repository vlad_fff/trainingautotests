package mainTests.yandex.ui.navigation;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static mainTests.ConsoleLogger.logToConsole;
import static mainTests.yandex.ui.WaitersAndJSExecutors.waitPageTitle;


public class NavigationYandexTest extends NavigationYandexBase {

    private static final String BASE_URL = "https://yandex.by/";


    @BeforeMethod
    public void beforeMethod() {
        driver.get(BASE_URL);

        waitPageTitle(driver, "Яндекс");
        logToConsole("Перешел на: %s", BASE_URL);
    }


    @Test
    public void linkNavigation() {
        homeArrowTabsNavigation(driver);
    }
}
