package sources.api.pojo.response.sw;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class PlanetsSW {

    @JsonProperty("count")
    private int count;
    @JsonProperty("next")
    private String next;
    @JsonProperty("previous")
    private String previous;
    @JsonProperty("results")
    private List<PlanetSW> results;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<PlanetSW> getResults() {
        return results;
    }

    public void setResults(List<PlanetSW> results) {
        this.results = results;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanetsSW planets = (PlanetsSW) o;
        return Objects.equals(count, planets.count) &&
                Objects.equals(next, planets.next) &&
                Objects.equals(previous, planets.previous) &&
                Objects.equals(results, planets.results);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, next, previous, results);
    }


    @Override
    public String toString() {
        return "Planets{" +
                "count=" + count +
                ", next='" + next + '\'' +
                ", previous='" + previous + '\'' +
                ", results=" + results +
                '}';
    }
}
