package sources.api.pojo.response.yandex;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorYandexDisk {

    @JsonProperty("message")
    private String message;
    @JsonProperty("description")
    private String description;
    @JsonProperty("error")
    private String error;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ErrorYandexDisk that = (ErrorYandexDisk) o;
        return Objects.equals(message, that.message) &&
                Objects.equals(description, that.description) &&
                Objects.equals(error, that.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, description, error);
    }

    @Override
    public String toString() {
        return "ErrorYandexDisk{" +
                "message='" + message + '\'' +
                ", description='" + description + '\'' +
                ", error='" + error + '\'' +
                '}';
    }
}
