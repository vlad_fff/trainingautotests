package sources.api.pojo.response.yandex;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SystemFolders {

    @JsonProperty("applications")
    private String applications;
    @JsonProperty("downloads")
    private String downloads;


    public String getApplications() {
        return applications;
    }

    public void setApplications(String applications) {
        this.applications = applications;
    }

    public String getDownloads() {
        return downloads;
    }

    public void setDownloads(String downloads) {
        this.downloads = downloads;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemFolders that = (SystemFolders) o;
        return Objects.equals(applications, that.applications) &&
                Objects.equals(downloads, that.downloads);
    }

    @Override
    public int hashCode() {
        return Objects.hash(applications, downloads);
    }

    @Override
    public String toString() {
        return "SystemFolders{" +
                "applications='" + applications + '\'' +
                ", downloads='" + downloads + '\'' +
                '}';
    }
}
