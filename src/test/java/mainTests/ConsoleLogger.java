package mainTests;


import org.apache.log4j.Logger;

import java.util.Locale;

public class ConsoleLogger {

    private static Logger Log = Logger.getLogger(ConsoleLogger.class);


    public static void logToConsole(String message) {
        Log.info(message);
    }

    public static void logToConsole(String message, Object... args) {
        Log.info(String.format(Locale.ROOT, message, args));
    }
}
