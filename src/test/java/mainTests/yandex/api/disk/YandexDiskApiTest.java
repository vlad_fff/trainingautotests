package mainTests.yandex.api.disk;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static mainTests.ConsoleLogger.logToConsole;

public class YandexDiskApiTest extends YandexDiskApiBase {

    @BeforeClass
    public static void beforeClass() {
        logToConsole("------------- Начинаю API-тестирование Yandex Disk -------------");
    }

    @BeforeMethod
    public void beforeMethod() {
        stepNumber = 1;
    }


    @Test
    public void workWithFolder() {
        createFolderOnDisk("MyTestFolder");
        deleteFileOrFolderFromDisk("MyTestFolder", "MyTestFolder");
        checkFileOrFolderIsNotExist("MyTestFolder");
    }

    @Test
    public void workWithFolderAndFile() {
        createFolderOnDisk("MyTestFolder");
        uploadFileToDisk("MyTestFolder/testFile.docx");
        deleteFileOrFolderFromDisk("MyTestFolder/testFile.docx", "testFile.docx");
        deleteFileOrFolderFromDisk("MyTestFolder", "MyTestFolder");
    }

    @Test
    public void workWithFolderFileAndTrash() {
        createFolderOnDisk("MyTestFolder");
        uploadFileToDisk("MyTestFolder/testFile.docx");
        moveFileOrFolderToTrash("MyTestFolder/testFile.docx");
        restoreFileOrFolderFromTrash("testFile.docx");
        deleteFileOrFolderFromDisk("MyTestFolder", "MyTestFolder");
    }

    @Test
    public void checkTrashSizeInfo() {
        long startTrashSize = getDiskInformation().getTrashSize();

        createFolderOnDisk("MyTestFolder");
        uploadFileToDisk("MyTestFolder/testFile1.docx");
        uploadFileToDisk("MyTestFolder/testFile2.docx");

        long testFileSize1 = getFileOrFolderMetadataInfo("MyTestFolder/testFile1.docx").getSize();
        long testFileSize2 = getFileOrFolderMetadataInfo("MyTestFolder/testFile2.docx").getSize();

        moveFileOrFolderToTrash("MyTestFolder/testFile1.docx");
        moveFileOrFolderToTrash("MyTestFolder/testFile2.docx");

        long endTrashSize = getDiskInformation().getTrashSize();

        checkTrashSizeChanging(startTrashSize, endTrashSize, testFileSize1, testFileSize2);

        restoreFileOrFolderFromTrash("testFile1.docx");
        restoreFileOrFolderFromTrash("testFile2.docx");

        deleteFileOrFolderFromDisk("MyTestFolder/testFile1.docx", "testFile1.docx");
        deleteFileOrFolderFromDisk("MyTestFolder/testFile2.docx", "testFile2.docx");
        deleteFileOrFolderFromDisk("MyTestFolder", "MyTestFolder");
    }

    @Ignore
    @Test(enabled = false)
    public void preTestingTrashSizeChanging() {
        long startTrashSize = getDiskInformation().getTrashSize();
        long fileSize1 = getFileOrFolderMetadataInfo("Winter.jpg").getSize();
        long fileSize2 = getFileOrFolderMetadataInfo("Sea.jpg").getSize();
        moveFileOrFolderToTrash("Winter.jpg");
        moveFileOrFolderToTrash("Sea.jpg");
        long endTrashSize = getDiskInformation().getTrashSize();
        checkTrashSizeChanging(startTrashSize, endTrashSize, fileSize1, fileSize2);
    }

    @Test
    public void checkPropertyTypes() {
        createFolderOnDisk("test");
        createFolderOnDisk("test/foo");
        uploadFileToDisk("test/foo/autotest");

        checkingPropertyTypesByMetaData("test", "test", "dir", "test");
        checkingPropertyTypesByMetaData("test/foo", "foo", "dir", "test/foo");
        checkingPropertyTypesByMetaData("test/foo/autotest", "autotest", "file", "test/foo/autotest");

        deleteFileOrFolderFromDisk("test", "test");

        checkFileOrFolderIsNotExist("test/foo/autotest");
        checkFileOrFolderIsNotExist("test/foo");
        checkFileOrFolderIsNotExist("test");
    }

    @Test
    public void checkTrashCleaning() {
        createFolderOnDisk("test");
        createFolderOnDisk("test/foo");
        uploadFileToDisk("test/foo/autotest");
        moveFileOrFolderToTrash("test");


//TODO: Пересмотреть. Тест порой падает, т.к. корзина не успевает очиститься
        //deleteFileOrFolderFromTrash("test");
        fullClearTrash();
        //checkSuccessStatus(link, "success");

        checkTrashIsEmpty(0);
    }
}
