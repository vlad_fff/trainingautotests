package sources.api.pojo.response.yandex;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DiskInfoYandexDisk {

    @JsonProperty("trash_size")
    private long trashSize;
    @JsonProperty("total_space")
    private long totalSpace;
    @JsonProperty("used_space")
    private long usedSpace;
    @JsonProperty("system_folders")
    private SystemFolders systemFolders;


    public long getTrashSize() {
        return trashSize;
    }

    public void setTrashSize(long trashSize) {
        this.trashSize = trashSize;
    }

    public long getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(long totalSpace) {
        this.totalSpace = totalSpace;
    }

    public long getUsedSpace() {
        return usedSpace;
    }

    public void setUsedSpace(long usedSpace) {
        this.usedSpace = usedSpace;
    }

    public SystemFolders getSystemFolders() {
        return systemFolders;
    }

    public void setSystemFolders(SystemFolders systemFolders) {
        this.systemFolders = systemFolders;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiskInfoYandexDisk that = (DiskInfoYandexDisk) o;
        return trashSize == that.trashSize &&
                totalSpace == that.totalSpace &&
                usedSpace == that.usedSpace &&
                Objects.equals(systemFolders, that.systemFolders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trashSize, totalSpace, usedSpace, systemFolders);
    }

    @Override
    public String toString() {
        return "DiskInfoYandexDisk{" +
                "trashSize=" + trashSize +
                ", totalSpace=" + totalSpace +
                ", usedSpace=" + usedSpace +
                ", systemFolders=" + systemFolders +
                '}';
    }
}
