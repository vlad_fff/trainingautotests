package sources.api;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import sources.api.pojo.response.yandex.*;

import static io.restassured.RestAssured.given;

public class YandexDiskApi {

    private static final String API_BASE_URL = "https://cloud-api.yandex.net";
    private static final String TOKEN_TYPE_OAUTH = "OAuth ";
    private static final String YANDEX_DISK_TOKEN = "AgAAAAAjLxXxAAY2MEcqhzzpGELpmEMoy4ihL3k";


    /**
     * Создание папки на диске по указанному пути + имя файла
     * @param path
     * @return
     */
    public static LinkYandexDisk createFolder(String path) {
        return given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk/resources")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                    .queryParam("path", path)
                .when()
                    .put()
                .then().statusCode(HttpStatus.SC_CREATED).extract().as(LinkYandexDisk.class);
    }

    /**
     * Удаление(перемещение) файла/папки в корзину
     * @param path
     * @return
     */
    public static LinkYandexDisk deleteFileOrFolderToTrash(String path) {
        Response response =
                given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk/resources")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                    .queryParam("path", path)
                .when()
                    .delete()
                .then().extract().response();

        if (response.statusCode() == HttpStatus.SC_NO_CONTENT)
            return null;

        return response.as(LinkYandexDisk.class);
    }

    /**
     * Удаление файла/папки из корзины
     * @param path
     * @return
     */
    public static LinkYandexDisk deleteFileOrFolderFromTrash(String path) {
        Response response =
                given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk/trash/resources")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                    .queryParam("path", path)
                .when()
                    .delete()
                .then().extract().response();

        if (response.statusCode() == HttpStatus.SC_NO_CONTENT)
            return null;

        return response.as(LinkYandexDisk.class);
    }

    /**
     * Попытка получить информацию о несуществующем файле/папке (генерация ошибки 404)
     * @param path
     * @return
     */
    public static ErrorYandexDisk getInfoNoExistFileOrFolder(String path) {
/*        RequestSpecification requestSpec;

        if (trash) {
            requestSpec = new RequestSpecBuilder()
                    .setBaseUri(API_BASE_URL)
                    .setBasePath("/v1/disk/trash/resources")
                    .setAccept("application/json")
                    .setContentType("application/json")
                    .addHeader("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                    .addQueryParam("path", path)
                    .build();
        } else {
            requestSpec = new RequestSpecBuilder()
                    .setBaseUri(API_BASE_URL)
                    .setBasePath("/v1/disk/resources")
                    .setAccept("application/json")
                    .setContentType("application/json")
                    .addHeader("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                    .addQueryParam("path", path)
                    .build();
        }

        return given()
                    .spec(requestSpec)
                .when()
                    .get()
                .then().statusCode(HttpStatus.SC_NOT_FOUND).extract().as(ErrorYandexDisk.class);*/
        return given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk/resources")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                    .queryParam("path", path)
                .when()
                    .get()
                .then().statusCode(HttpStatus.SC_NOT_FOUND).extract().as(ErrorYandexDisk.class);
    }

    /**
     * Делаем запрос на загрузку файла
     * @param path
     * @return
     */
    public static LinkYandexDisk getUploadURL(String path) {
        return given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk/resources/upload")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                    .queryParam("path", path)
                    .queryParam("overwrite", true)
                .when()
                    .get()
                .then().statusCode(HttpStatus.SC_OK).extract().as(LinkYandexDisk.class);
    }

    /**
     * Делаем запрос на загрузку файла
     * @param path
     * @return
     */
    public static void uploadFile(String path) {
        given()
            .baseUri(path)
            .accept("application/json")
            .contentType("application/json")
            .queryParam("path", path)
        .when()
            .put()
        .then().statusCode(HttpStatus.SC_CREATED);
    }



    /**
     * Полная очистка корзины
     * @return
     */
    public static LinkYandexDisk clearTrash() {
        Response response =
                given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk/trash/resources")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                .when()
                    .delete()
                .then().extract().response();


        if (response.statusCode() == HttpStatus.SC_NO_CONTENT ||
                response.statusCode() == HttpStatus.SC_ACCEPTED) {

            if (waitTrashTotal(0))
                return response.as(LinkYandexDisk.class);
        }

        ErrorYandexDisk error = response.as(ErrorYandexDisk.class);

        System.out.println("\n------------------------------------------");
        System.out.println("Message: " + error.getMessage());
        System.out.println("Error: " + error.getError());
        System.out.println("Description: " + error.getDescription());
        System.out.println("\n------------------------------------------");

        throw new RuntimeException("Ошибка при очистке корзины. Код ответа: " + response.statusCode());
    }

    /**
     * Ожидание полной очистки корзины
     * @param expectedTotal
     * @return
     */
    public static boolean waitTrashTotal(int expectedTotal) {
        int timeout = 1000;
        int repair = 10;

        try {
            do {
                int trashTotal = getTrashMetadata().getEmbedded().getTotal();

                if (trashTotal == expectedTotal)
                    return true;

                repair--;
                Thread.sleep(timeout);
            } while (repair != 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Размер корзины не изменился");
    }



    /**
     * Восстановление файла/папки из корзины
     * @param path
     * @return
     */
    public static LinkYandexDisk restoreFileOrFolder(String path) {
        return given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk/trash/resources/restore")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                    .queryParam("path", path)
                .when()
                    .put()
                .then().statusCode(HttpStatus.SC_CREATED).extract().as(LinkYandexDisk.class);
    }

    /**
     *
     * @param path
     * @param trash true - если хотим получить метаданные корзины;
     *              false - если хотим получить метаданные файла или папки.
     * @return
     */
    /*public static ResourceYandexDisk getMetadata(String path, boolean trash) {

        RequestSpecification requestSpec;

        if (trash) {
            requestSpec = new RequestSpecBuilder()
                                .setBaseUri(API_BASE_URL)
                                .setBasePath("/v1/disk/trash/resources")
                                .setAccept("application/json")
                                .setContentType("application/json")
                                .addHeader("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                                .build();
        } else {
            requestSpec = new RequestSpecBuilder()
                                .setBaseUri(API_BASE_URL)
                                .setBasePath("/v1/disk/resources")
                                .setAccept("application/json")
                                .setContentType("application/json")
                                .addHeader("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                                .build();
        }

        return given()
                    .spec(requestSpec)
                    .queryParam("path", path)
                .when()
                    .get()
                .then().statusCode(HttpStatus.SC_OK).extract().as(ResourceYandexDisk.class);
    }*/

    /**
     * Получаем метаданные ресурса (файла/папки)
     * @param path
     */
    public static ResourceYandexDisk getFileOrFolderMetadata(String path) {
        return given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk/resources")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                    .queryParam("path", path)
                .when()
                    .get()
                .then().statusCode(HttpStatus.SC_OK).extract().as(ResourceYandexDisk.class);
    }

    /**
     * Получаем метаданные корзины
     */
    public static ResourceYandexDisk getTrashMetadata() {
        return given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk/trash/resources")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                .when()
                    .get()
                .then().statusCode(HttpStatus.SC_OK).extract().as(ResourceYandexDisk.class);
    }

    /**
     * Получаем информацию о диске пользователя
     * @return
     */
    public static DiskInfoYandexDisk getDiskInfo() {
        return given()
                    .baseUri(API_BASE_URL)
                    .basePath("/v1/disk")
                    .accept("application/json")
                    .contentType("application/json")
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                .when()
                    .get()
                .then().statusCode(HttpStatus.SC_OK).extract().as(DiskInfoYandexDisk.class);
    }

    /**
     * Получение статуса операции по ссылке (ID) операции
     * @param link
     * @return
     */
    public static OperationYandexDisk checkOperationStatus(String link) {
        return given()
                    .baseUri(link)
                    .header("Authorization", TOKEN_TYPE_OAUTH.concat(YANDEX_DISK_TOKEN))
                .when()
                    .get()
                .then().statusCode(HttpStatus.SC_OK).extract().as(OperationYandexDisk.class);
    }

}
